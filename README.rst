======================================
Génération d'emploi du temps pour l'UA
======================================


Voir rapport de stage pour une explication plus complète du projet :  :download:`pdf <rapport.pdf>`

Dans ce projet, nous avons développé 3 méthodes pour résoudre l'emplois du temps, un modèle PPC, une LNS et une recherche locale.

Dans le dossier emploisdutemps, vous pouvez trouver :

- docs : la documentation du projet
- drawio : les schémas créés avec drawio
- edt : le code développé pour le projet
   * distribution.py : gestion des contraintes de répartition
   * instance.py : gestion de l'instance
   * instance_objects.py : objets utilisés dans le projet
   * local_search.py : recherche locale pour la génération de l'emplois du temps
   * parse_xml_objects.py : parser des fichiers XML du concours ITC2019
   * ppc.py : gestion du modèle PPC et LNS avec minizinc
   * student_scetionning.py : différentes implémentations pour gérer le student sectioning (beaucoup de code est commenté car certains outils ne fonctionnent pas avec Pypy)
   * tools.py : différentes fonctions utilisées dans le projet
   * web.py : gestion de l'affichage de l'emplois du temps sur le site dans le dossier theleme
- images : un dossier où étaient enregistrés les résultats du student sectioning avec la recherche locale
- mzn\_files : dossier des fichiers minizinc
   * dzn : output des fichiers dzn
   * mzc : dossier pour les fichiers mzc (à générer à la main...)
   * \*.mzn : différents modèles
- output\_tests\_* : résultats des tests
- ressources : dossier des différentes instance de l'ITC2019 et de la L3 informatique d'Angers
- tests : différents tests (à développer)
- theleme : dossier du site pour afficher et modifier les instances
- cli\_run\_*, *.sh et \*test\*.py : gestion des tests
- main.py : fichier main avec les différentes possibilités de lancement du projet sur les différentes méthodes
- Makefile : différentes commandes pour nettoyer le projet, faire la doc, installer des éléments,...
- reparation.py : 3 scripts de réparation
- requirements.txt : liste des librairies utilisées



* Free software: MIT license
* Documentation: https://cyril_grl.gitlab.io/emploisdutemps/.

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
