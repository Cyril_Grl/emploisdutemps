from edt.instance import get_instance
from edt.instance_objects import Time


def test_times_positions():
    """
    Test positions of days and weeks
    """
    t1 = Time("1000000", 90, 10, "1000", 0)
    assert t1.days == [0]
    assert t1.weeks == [0]
    t1 = Time("1110000", 90, 10, "1001", 0)
    assert t1.days == [0, 1, 2]
    assert t1.weeks == [0, 3]


def test_times_eq():
    t1 = Time("1000000", 90, 10, "1000", 0)
    t2 = Time("1000000", 90, 10, "1000", 0)
    assert t1 == t2
    assert t1.same_time(t2)
    assert not t1.before(t2)
    t2 = Time("1000000", 95, 10, "1000", 0)
    assert t1 != t2
    assert t1.same_time(t2)
    assert not t1.before(t2)
    t2 = Time("0100010", 95, 10, "1000", 0)
    assert t1 != t2
    assert not t1.same_time(t2)
    assert t1.before(t2)
    t1 = Time("1100000", 90, 10, "1000", 0)
    assert t1 != t2
    assert t1.same_time(t2)
    assert t1.before(t2)
    t1 = Time("1100000", 90, 10, "0100", 0)
    t2 = Time("0100000", 90, 10, "1000", 0)
    assert t1 != t2
    assert not t1.same_time(t2)
    assert not t1.before(t2)
    t1 = Time("1101000", 90, 10, "0110", 0)
    t2 = Time("0001110", 90, 10, "1100", 0)
    assert t1 != t2
    assert t1.same_time(t2)
    assert not t1.before(t2)
    t1 = Time("1101000", 90, 10, "0110", 0)
    t2 = Time("0001110", 95, 10, "1100", 0)
    assert t1 != t2
    assert t1.same_time(t2)
    assert not t1.before(t2)
    t1 = Time("1101000", 90, 10, "0110", 0)
    t2 = Time("0001110", 150, 10, "1100", 0)
    assert t1 != t2
    assert not t1.same_time(t2)
    assert not t1.before(t2)


def test_students():
    ins = get_instance("ressources/tests/valid_sol1.xml")
    assert len(ins.students) == 2
