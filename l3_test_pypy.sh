#!/bin/bash

source /home/user/Documents/venv/bin/activate

nb_run=50

for i in $(seq 1 $nb_run); do
  echo --method ls --randseed $i --light 1 --rand_init 0  --recuit 0
  pypy3 cli_run_l3.py --method ls --randseed $i --light 1 --rand_init 0  --recuit 0
done

for i in $(seq 1 $nb_run); do
  echo --method ls --randseed $i --light 1 --rand_init 0  --recuit 1
  pypy3 cli_run_l3.py --method ls --randseed $i --light 1 --rand_init 0 --recuit 1
done

for i in $(seq 1 $nb_run); do
  echo --method ls --randseed $i --light 1 --rand_init 1 --recuit 0
  pypy3 cli_run_l3.py --method ls --randseed $i --light 1 --rand_init 1 --recuit 0
done

for i in $(seq 1 $nb_run); do
  echo --method ls --randseed $i --light 1 --rand_init 1 --recuit 1
  pypy3 cli_run_l3.py --method ls --randseed $i --light 1 --rand_init 1 --recuit 1
done

for i in $(seq 1 $nb_run); do
  echo --method lns --randseed $i --light 1 --rand_init 0
  pypy3 cli_run_l3.py --method lns --randseed $i --light 1 --rand_init 0
done

for i in $(seq 1 $nb_run); do
  echo --method lns --randseed $i --light 0 --rand_init 0
  pypy3 cli_run_l3.py --method lns --randseed $i --light 0 --rand_init 0
done
