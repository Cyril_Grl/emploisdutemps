"""
Lecture des fichiers xml
"""
from typing import List, Dict
from xml.etree.ElementTree import parse, Element

from edt.distributions import get_object_from_str, Distribution
from edt.instance_objects import (
    Class,
    Config,
    Course,
    Optimization,
    Problem,
    Room,
    Student,
    Subpart,
    Time,
)


def get_optimization_object(child: Element) -> Optimization:
    """Récupère les informations sur les poids du problème

    Args:
        child (Element): Balise XML

    Returns:
        Optimization: Poids du problème
    """
    optimization = child.attrib
    time = int(optimization["time"])
    room = int(optimization["room"])
    distribution = int(optimization["distribution"])
    student = int(optimization["student"])
    return Optimization(time, room, distribution, student)


def get_rooms_object(child: Element) -> Dict[str, Room]:
    """Récupère les salles de classes

    Args:
        child (Element): balise XML

    Returns:
        Dict[str, Room]: dictionnaire des salles
    """
    rooms_names = ["noroom"] + [room.attrib["id"] for room in child]
    rooms = {"noroom": Room("noroom", 5000, [], [(rn, 0) for rn in rooms_names])}
    for room in child:
        travel = [(rn, 0) for rn in rooms_names]
        unavailable = []
        for sub in room:
            if sub.tag == "travel":
                for i, rn_val in enumerate(travel):
                    r_n, _ = rn_val
                    if r_n == sub.attrib["room"]:
                        travel[i] = (sub.attrib["room"], int(sub.attrib["value"]))
                # travel.append((sub.attrib['room'], int(sub.attrib['value'])))
            elif sub.tag == "unavailable":
                unavailable.append(
                    Time(
                        sub.attrib["days"],
                        int(sub.attrib["start"]),
                        int(sub.attrib["length"]),
                        sub.attrib["weeks"],
                    )
                )
        rooms[room.attrib["id"]] = Room(
            room.attrib["id"], int(room.attrib["capacity"]), unavailable, travel
        )

    for rid, room in rooms.items():
        for travel_1 in room.travel:
            if travel_1[1] != 0:
                for i, travel_2 in enumerate(rooms[travel_1[0]].travel):
                    if travel_2[0] == rid:
                        rooms[travel_1[0]].travel[i] = (rid, travel_1[1])
    return rooms


def get_courses_object(
    child: Element, allrooms: Dict[str, Room], no_student: bool
) -> Dict[str, Course]:
    """Récupère l'ensemble des cours

    Args:
        child (Element): balise XML
        allrooms (Dict[str, Room]): Ensemble des salles
        no_student (boo1): True si aucun étudiant dans l'instance

    Raises:
        Exception: Si problème dans le fichier XML

    Returns:
        Dict[str, Course]: Dictionnaire des cours
    """
    courses = {}
    for course in child:
        data = {}
        for config in course:
            conf = {}
            for subpart in config:
                subp = {}
                for clas in subpart:
                    rooms = []
                    times = []
                    for classe in clas:
                        if classe.tag == "room":
                            rooms.append(
                                (
                                    allrooms[classe.attrib["id"]],
                                    int(classe.attrib["penalty"]),
                                )
                            )
                        elif classe.tag == "time":
                            times.append(
                                Time(
                                    classe.attrib["days"],
                                    int(classe.attrib["start"]),
                                    int(classe.attrib["length"]),
                                    classe.attrib["weeks"],
                                    int(classe.attrib["penalty"]),
                                )
                            )
                    parent = None
                    if "parent" in clas.attrib:
                        for _, sub in conf.items():
                            for classid, class_parent in sub:
                                if classid == clas.attrib["parent"]:
                                    parent = class_parent
                        if parent is None:
                            raise Exception(
                                f'problem : parent : {clas.attrib["parent"]}'
                                f'for class {clas.attrib["id"]} doesn\'t exist'
                            )
                    if "room" in clas.attrib:
                        rooms.append((allrooms["noroom"], 0))
                    subp[clas.attrib["id"]] = Class(
                        clas.attrib["id"],
                        int(clas.attrib["limit"]),
                        parent,
                        rooms,
                        times,
                        "room" not in clas.attrib,
                    )
                conf[subpart.attrib["id"]] = Subpart(subpart.attrib["id"], subp)
                for _, classe in conf[subpart.attrib["id"]].classes.items():
                    classe.subpart = conf[subpart.attrib["id"]]
            data[config.attrib["id"]] = Config(config.attrib["id"], conf)
            for _, subpart in data[config.attrib["id"]].subparts.items():
                subpart.config = data[config.attrib["id"]]
        courses[course.attrib["id"]] = Course(course.attrib["id"], data, no_student)
        for _, classe in courses[course.attrib["id"]].configs.items():
            classe.course = courses[course.attrib["id"]]
    return courses


def get_distributions_object(
    child: Element, courses: Dict[str, Course]
) -> List[Distribution]:
    """Récupère les contraintes de répartition

    Args:
        child (Element): balise XML
        courses (Dict[str, Course]): Dictionnaire des cours

    Raises:
        Exception: balise inconnue

    Returns:
        List[Distribution]: Liste des contraintes
    """
    classes_ptr = dict()
    for _, course in courses.items():
        for _, config in course:
            for _, subpart in config:
                for c_id, classe in subpart:
                    classes_ptr[c_id] = classe
    distributions = []
    for distribution in child:
        classes = []
        for clas in distribution:
            classes.append(classes_ptr[clas.attrib["id"]])
        required = False
        penalty = -1
        if "required" in distribution.attrib:
            required = True
        if "penalty" in distribution.attrib:
            penalty = int(distribution.attrib["penalty"])
        tmp = distribution.attrib
        tmp.update({"classes": classes})
        type_ = distribution.attrib["type"]
        if "(" in type_:
            nom = type_[:-1].split("(")
            type_ = nom[0]
            nom = nom[1].split(",")
            distri_obj = get_object_from_str(type_)
            if len(nom) == 1:
                distributions.append(
                    distri_obj(type_, required, penalty, classes, int(nom[0]))
                )
            elif len(nom) == 2:
                distri_obj = get_object_from_str(type_)
                distributions.append(
                    distri_obj(
                        type_, required, penalty, classes, int(nom[0]), int(nom[1])
                    )
                )
            else:
                raise Exception(f"problème distribution : {distribution}")
        else:
            distri_obj = get_object_from_str(type_)
            distributions.append(distri_obj(type_, required, penalty, classes))
    for distribution in distributions:
        for classe in distribution.classes:
            classe.in_distri.append(distribution)
    return distributions


def get_students_object(
    child: Element, courses: Dict[str, Course]
) -> Dict[str, Student]:
    """Récupère les étudiants

    Args:
        child (Element): balise XML
        courses (Dict[str, Course]): dictionnaire des cours

    Returns:
        Dict[str, Student]: dictionnaire des étudiants
    """
    students = {}
    for student in child:
        classes = []
        for clas in student:
            classes.append(courses[clas.attrib["id"]])
        students[student.attrib["id"]] = Student(student.attrib["id"], classes)
    return students


def parse_file_object(file: str) -> dict:
    """Parse le fichier xml donné

    Args:
        file (str): fichier xml à parser

    Raises:
        Exception: retourne une exception si balise inconnue

    Returns:
        dict: Dictionnaire des informations du fichier XML
    """
    tree = parse(file)
    problem = tree.getroot()

    problem_ = Problem(
        problem.attrib["name"],
        int(problem.attrib["nrDays"]),
        int(problem.attrib["nrWeeks"]),
        int(problem.attrib["slotsPerDay"]),
        file,
    )

    optimization = None
    rooms = dict()
    courses = dict()
    distributions = list()
    students = dict()

    no_student = False

    for child in problem:
        if child.tag == "students":
            if len(child) == 0:
                no_student = True

    for child in problem:
        if child.tag == "optimization":
            optimization = get_optimization_object(child)
        elif child.tag == "rooms":
            rooms = get_rooms_object(child)
        elif child.tag == "courses":
            courses = get_courses_object(child, rooms, no_student)
        elif child.tag == "distributions":
            distributions = get_distributions_object(child, courses)
        elif child.tag == "students":
            students = get_students_object(child, courses)
        else:
            raise Exception(f"Balise non reconnue : {child.tag}")

    return {
        "problem": problem_,
        "optimization": optimization,
        "rooms": rooms,
        "courses": courses,
        "distributions": distributions,
        "students": students,
    }
