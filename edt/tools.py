"""
Some tools
"""
from typing import List, Generator


def pgcd(i: int, j: int) -> int:
    """PGCD de i et j

    Args:
        i (int): entier
        j (int): entier

    Returns:
        int: PGCD de i et j
    """
    while j != 0:
        i, j = j, i % j
    return i


def pgcdn(liste: List[int]) -> int:
    """PGCD sur liste d'entiers

    Args:
        liste (List[int]): liste d'entiers

    Returns:
        int: PGCD de la liste
    """
    i = pgcd(liste[0], liste[1])
    for j in liste[2:]:
        i = pgcd(i, j)
    return i


def chunks(lst: list, j: int) -> Generator[list, None, None]:
    """Coupe la liste lst en blocs de taille n

    Args:
        lst (list): la liste à couper
        n (int): le nombre de morceaux

    Yields:
        Generator[list, None, None]: les morceaux de liste
    """
    for i in range(0, len(lst), j):
        yield lst[i : i + j]


# def save_images(numero: int) -> None:
#     """Création d'une video avec les images contenues dans le dossier images
#     Ne fonctionne pas avec Pypy

#     Args:
#         numero (int): titre de la video
#     """
#     import os
#     import cv2
#     image_folder = 'images'
#     video_name = f'video_{numero}.avi'
#     images = [img for img in os.listdir(image_folder) if
#               img.endswith(".png")]
#     images.sort()
#     frame = cv2.imread(os.path.join(image_folder, images[0]))
#     height, width, layers = frame.shape
#     video = cv2.VideoWriter(video_name, 0, 1, (width, height))
#     for image in images:
#         video.write(cv2.imread(os.path.join(image_folder, image)))
#     cv2.destroyAllWindows()
#     video.release()
