.PHONY: clean clean-test clean-pyc clean-build docs help
.DEFAULT_GOAL := help

VENV_NAME?=venv
VENV_ACTIVATE=. $(VENV_NAME)/bin/activate
PYTHON=${VENV_NAME}/bin/python3

prepare-dev:
	sudo apt-get -y install python3.8
	sudo apt-get -y install python3-pip
	python3 -m pip install virtualenv
	virtualenv -p python3.8 venv
	source venv/bin/activate
	pip install -r requirements.txt

# Requirements are in setup.py, so whenever setup.py is changed, re-run installation of dependencies.

#$(VENV_NAME)/bin/activate: setup.py
#	test -d $(VENV_NAME) || virtualenv -p python3 $(VENV_NAME)
#	${PYTHON} -m pip install -U pip
#	${PYTHON} -m pip install -e .
#	touch $(VENV_NAME)/bin/activate
#venv: $(VENV_NAME)/bin/activate

define BROWSER_PYSCRIPT
import os, webbrowser, sys

from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

BROWSER := ${PYTHON} -c "$$BROWSER_PYSCRIPT"

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: clean-build clean-pyc clean-test ## remove all build, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr .pytest_cache

lint: ## check style with flake8
	${PYTHON} -m flake8 edt tests

test: ## run tests quickly with the default Python
	${PYTHON} -m pytest

test-all: ## run tests on every Python version with tox
	${PYTHON} -m black edt
	${PYTHON} -m tox

coverage: ## check code coverage quickly with the default Python
	${PYTHON} -m coverage run --source edt -m pytest
	${PYTHON} -m coverage report -m
	${PYTHON} -m coverage html
	$(BROWSER) htmlcov/index.html

docs:## generate Sphinx HTML documentation, including API docs
	rm -f docs/edt.rst
	rm -f docs/modules.rst
	echo '$(VENV_ACTIVATE)'
	$(VENV_ACTIVATE); sphinx-apidoc -o docs/ edt
	$(VENV_ACTIVATE); $(MAKE) -C docs clean
	$(VENV_ACTIVATE); $(MAKE) -C docs html
	$(VENV_ACTIVATE); $(BROWSER) docs/_build/html/index.html

#servedocs: docs ## compile the docs watching for changes
#	watchmedo shell-command -p '*.rst' -c '$(MAKE) -C docs html' -R -D .

#release: dist ## package and upload a release
#	twine upload dist/*

#dist: clean ## builds source and wheel package
#	python setup.py sdist
#	python setup.py bdist_wheel
#	ls -l dist

#install: clean ## install the package to the active Python's site-packages
#	python setup.py install
