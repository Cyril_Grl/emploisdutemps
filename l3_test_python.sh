#!/bin/bash

source venv/bin/activate

nb_run=50

for i in $(seq 1 $nb_run); do
  echo python cli_run.py --method ls --randseed $i --light 1 --rand_init 0
  python cli_run.py --method ls --randseed $i --light 1 --rand_init 0
done

#for i in $(seq 1 $nb_run); do
#  echo python cli_run.py --method ls --randseed $i --light 1 --rand_init 1
#  python cli_run.py --method ls --randseed $i --light 1 --rand_init 1
#done
#
#for i in $(seq 1 $nb_run); do
#  echo python cli_run.py --method ls --randseed $i --light 1 --rand_init 1 --recuit 0
#  python cli_run.py --method ls --randseed $i --light 1 --rand_init 0 --recuit 1
#done
#
#for i in $(seq 1 $nb_run); do
#  echo python cli_run.py --method ls --randseed $i --light 1 --rand_init 1 --recuit 1
#  python cli_run.py --method ls --randseed $i --light 1 --rand_init 1 --recuit 1
#done
#
#for i in $(seq 1 $nb_run); do
#  echo python cli_run.py --method lns --randseed $i --light 1 --rand_init 0
#  python cli_run.py --method lns --randseed $i --light 1 --rand_init 0
#done
#
#for i in $(seq 1 $nb_run); do
#  echo python cli_run.py --method lns --randseed $i --light 0 --rand_init 0
#  python cli_run.py --method lns --randseed $i --light 0 --rand_init 0
#done
