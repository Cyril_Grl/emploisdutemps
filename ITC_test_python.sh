#!/bin/bash

source venv/bin/activate

nb_run=2
#"tg-fal17.xml" "tg-spr18.xml" "iku-spr18.xml" "lums-spr18.xml"
#  "lums-fal17.xml" "bet-sum18.xml" "lums-sum17.xml" "iku-fal17.xml"
files=("muni-fspsx-fal17.xml" "agh-ggis-spr17.xml" "muni-fi-spr17.xml"
  "wbg-fal10.xml" "muni-pdfx-fal17.xml" "yach-fal17.xml" "muni-fsps-spr17c.xml"
  "pu-cs-fal07.xml" "muni-pdf-spr16.xml" "agh-ggos-spr17.xml")

for file in "${files[@]}"; do
  for i in $(seq 1 $nb_run); do
    echo cli_run_ITC.py --file $file --randseed $i --rand_init 0 --recuit 0
    python cli_run_ITC.py --file $file --randseed $i --rand_init 0 --recuit 0

    echo cli_run_ITC.py --file $file --randseed $i --rand_init 0 --recuit 1
    python cli_run_ITC.py --file $file --randseed $i --rand_init 0 --recuit 1

    echo cli_run_ITC.py --file $file --randseed $i --rand_init 1 --recuit 1
    python cli_run_ITC.py --file $file --randseed $i --rand_init 1 --recuit 1

  done
done
