#/bin/sh
# transforme les rangs des créneaux en minutes : rang 1 <=> 8h <=> 97 minutes pour interface web !

xd=5;
xvd=288;
xL=1;
xvL=16;
xs1=1;
xv1=97;
xs2=2;
xv2=115;
xs3=3;
xv3=133;
xs4=4;
xv4=169;
xs5=5;
xv5=187;

find . -type f -name "solution.xml" -print0 | xargs -0 sed -i -E `echo "s/slotsPerDay=\"$xd\"/slotsPerDay=\"$xvd\"/"`;
find . -type f -name "solution.xml" -print0 | xargs -0 sed -i -E `echo "s/length=\"$xL\"/length=\"$xvL\"/"`;
find . -type f -name "solution.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs1\"/start=\"$xv1\"/"`;
find . -type f -name "solution.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs2\"/start=\"$xv2\"/"`;
find . -type f -name "solution.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs3\"/start=\"$xv3\"/"`;
find . -type f -name "solution.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs4\"/start=\"$xv4\"/"`;
find . -type f -name "solution.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs5\"/start=\"$xv5\"/"`;
