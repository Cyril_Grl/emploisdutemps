#/bin/sh
# transforme les rangs des créneaux en minutes : rang 1 <=> 8h <=> 97 minutes pour interface web !

xd=5;
xvd=288;
xL=1;
xvL=16;
xs1=1;
xv1=97;
xs2=2;
xv2=115;
xs3=3;
xv3=133;
xs4=4;
xv4=169;
xs5=5;
xv5=187;

cp ../emploisdutemps/solution_1819-l3info-s2.xml delete.xml;

find . -type f -name "delete.xml" -print0 | xargs -0 sed -i -E "s/<solution[^<]*//";
find . -type f -name "delete.xml" -print0 | xargs -0 sed -i -E "s/\&\#233;/é/g";
find . -type f -name "delete.xml" -print0 | xargs -0 sed -i -E "s/\&\#232;/è/g";
cat ./src/edt-problem-1819-l3info-s2-all-preamble.xml ./src/edt-problem-1819-l3info-s2-all-sol.txt delete.xml > delete-1.xml;

cp delete-1.xml solution-for-web-ui.xml;
find . -type f -name "delete1.xml" -print0 | xargs -0 sed -E "s/^.*student.*$//g" > delete-2.xml;

find . -type f -name "solution-for-web-ui.xml" -print0 | xargs -0 sed -i -E `echo "s/slotsPerDay=\"$xd\"/slotsPerDay=\"$xvd\"/"`;
find . -type f -name "solution-for-web-ui.xml" -print0 | xargs -0 sed -i -E `echo "s/length=\"$xL\"/length=\"$xvL\"/g"`;
find . -type f -name "solution-for-web-ui.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs1\"/start=\"$xv1\"/g"`;
find . -type f -name "solution-for-web-ui.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs2\"/start=\"$xv2\"/g"`;
find . -type f -name "solution-for-web-ui.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs3\"/start=\"$xv3\"/g"`;
find . -type f -name "solution-for-web-ui.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs4\"/start=\"$xv4\"/g"`;
find . -type f -name "solution-for-web-ui.xml" -print0 | xargs -0 sed -i -E `echo "s/start=\"$xs5\"/start=\"$xv5\"/g"`;

cp problem.xml ../web/dist/stageproject/app/XML/problem.xml;
cp solution-for-web-ui.xml ../web/dist/stageproject/app/XML/solution.xml;
